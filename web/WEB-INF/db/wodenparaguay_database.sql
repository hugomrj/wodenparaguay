--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.24
-- Dumped by pg_dump version 9.5.24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: aplicacion; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA aplicacion;


ALTER SCHEMA aplicacion OWNER TO postgres;

--
-- Name: sistema; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sistema;


ALTER SCHEMA sistema OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: desconexiones_recibidas; Type: TABLE; Schema: aplicacion; Owner: hugo
--

CREATE TABLE aplicacion.desconexiones_recibidas (
    id integer NOT NULL,
    cosap character varying,
    fecha date,
    franja_horaria character varying,
    gruop_id integer,
    cuenta character varying,
    documento character varying,
    nombre_suscriptor character varying,
    correo_suscriptor character varying,
    telefono_1 character varying,
    telefono_2 character varying,
    telefono_3 character varying,
    telefono_4 character varying,
    whatsapp character varying,
    direccion_instalacion character varying,
    nombre_nodo character varying,
    nombre_comunidad character varying,
    departamento character varying,
    pais character varying,
    serial_20 character varying,
    direccionable character varying,
    familia character varying,
    nombre_material character varying,
    fecha_dx character varying,
    motivo_desconexion character varying,
    observaciones character varying,
    tipo_cliente character varying,
    segmentacion character varying,
    tipo_base character varying,
    zona character varying,
    agente_campo character varying,
    nombre character varying,
    latitud character varying,
    longitud character varying,
    prioridad character varying,
    tipo_tarea character varying,
    sede character varying
);


ALTER TABLE aplicacion.desconexiones_recibidas OWNER TO hugo;

--
-- Name: desconexiones_recibidas_id_seq; Type: SEQUENCE; Schema: aplicacion; Owner: hugo
--

CREATE SEQUENCE aplicacion.desconexiones_recibidas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.desconexiones_recibidas_id_seq OWNER TO hugo;

--
-- Name: desconexiones_recibidas_id_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: hugo
--

ALTER SEQUENCE aplicacion.desconexiones_recibidas_id_seq OWNED BY aplicacion.desconexiones_recibidas.id;


--
-- Name: roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.roles (
    rol integer NOT NULL,
    nombre_rol character varying(140)
);


ALTER TABLE sistema.roles OWNER TO postgres;

--
-- Name: roles_rol_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.roles_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.roles_rol_seq OWNER TO postgres;

--
-- Name: roles_rol_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.roles_rol_seq OWNED BY sistema.roles.rol;


--
-- Name: roles_x_selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.roles_x_selectores (
    id integer NOT NULL,
    rol integer,
    selector integer
);


ALTER TABLE sistema.roles_x_selectores OWNER TO postgres;

--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.roles_x_selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.roles_x_selectores_id_seq OWNER TO postgres;

--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.roles_x_selectores_id_seq OWNED BY sistema.roles_x_selectores.id;


--
-- Name: selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.selectores (
    id integer NOT NULL,
    superior integer,
    descripcion character varying,
    ord integer,
    link character varying
);


ALTER TABLE sistema.selectores OWNER TO postgres;

--
-- Name: selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.selectores_id_seq OWNER TO postgres;

--
-- Name: selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.selectores_id_seq OWNED BY sistema.selectores.id;


--
-- Name: selectores_x_webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.selectores_x_webservice (
    id integer NOT NULL,
    selector integer,
    wservice integer
);


ALTER TABLE sistema.selectores_x_webservice OWNER TO postgres;

--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.selectores_x_webservice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.selectores_x_webservice_id_seq OWNER TO postgres;

--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.selectores_x_webservice_id_seq OWNED BY sistema.selectores_x_webservice.id;


--
-- Name: usuarios; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.usuarios (
    usuario integer NOT NULL,
    cuenta character varying(100),
    clave character varying(150),
    token_iat character varying
);


ALTER TABLE sistema.usuarios OWNER TO postgres;

--
-- Name: usuarios_usuario_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.usuarios_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.usuarios_usuario_seq OWNER TO postgres;

--
-- Name: usuarios_usuario_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.usuarios_usuario_seq OWNED BY sistema.usuarios.usuario;


--
-- Name: usuarios_x_roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.usuarios_x_roles (
    id integer NOT NULL,
    usuario integer,
    rol integer
);


ALTER TABLE sistema.usuarios_x_roles OWNER TO postgres;

--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.usuarios_x_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.usuarios_x_roles_id_seq OWNER TO postgres;

--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.usuarios_x_roles_id_seq OWNED BY sistema.usuarios_x_roles.id;


--
-- Name: webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.webservice (
    wservice integer NOT NULL,
    path character varying,
    nombre character varying
);


ALTER TABLE sistema.webservice OWNER TO postgres;

--
-- Name: webservice_wservice_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.webservice_wservice_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.webservice_wservice_seq OWNER TO postgres;

--
-- Name: webservice_wservice_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.webservice_wservice_seq OWNED BY sistema.webservice.wservice;


--
-- Name: id; Type: DEFAULT; Schema: aplicacion; Owner: hugo
--

ALTER TABLE ONLY aplicacion.desconexiones_recibidas ALTER COLUMN id SET DEFAULT nextval('aplicacion.desconexiones_recibidas_id_seq'::regclass);


--
-- Name: rol; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles ALTER COLUMN rol SET DEFAULT nextval('sistema.roles_rol_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores ALTER COLUMN id SET DEFAULT nextval('sistema.roles_x_selectores_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores ALTER COLUMN id SET DEFAULT nextval('sistema.selectores_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores_x_webservice ALTER COLUMN id SET DEFAULT nextval('sistema.selectores_x_webservice_id_seq'::regclass);


--
-- Name: usuario; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios ALTER COLUMN usuario SET DEFAULT nextval('sistema.usuarios_usuario_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles ALTER COLUMN id SET DEFAULT nextval('sistema.usuarios_x_roles_id_seq'::regclass);


--
-- Name: wservice; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.webservice ALTER COLUMN wservice SET DEFAULT nextval('sistema.webservice_wservice_seq'::regclass);


--
-- Data for Name: desconexiones_recibidas; Type: TABLE DATA; Schema: aplicacion; Owner: hugo
--

COPY aplicacion.desconexiones_recibidas (id, cosap, fecha, franja_horaria, gruop_id, cuenta, documento, nombre_suscriptor, correo_suscriptor, telefono_1, telefono_2, telefono_3, telefono_4, whatsapp, direccion_instalacion, nombre_nodo, nombre_comunidad, departamento, pais, serial_20, direccionable, familia, nombre_material, fecha_dx, motivo_desconexion, observaciones, tipo_cliente, segmentacion, tipo_base, zona, agente_campo, nombre, latitud, longitud, prioridad, tipo_tarea, sede) FROM stdin;
1	1234	\N	\N	\N	123	123	PEDRO PEREZ	\N	45645	45454	\N	\N	\N	DIRECCION 1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	222	\N	\N	\N	222	222	FSAFSAFSAD	\N	222	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	333	\N	\N	\N	333	333	GAGASGSAGA	\N	333	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	555	\N	\N	\N	555	555	AJFASJFASF	\N	555	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	444	\N	\N	\N	444	444	FASFASF	\N	444	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Name: desconexiones_recibidas_id_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: hugo
--

SELECT pg_catalog.setval('aplicacion.desconexiones_recibidas_id_seq', 5, true);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.roles (rol, nombre_rol) FROM stdin;
9	SysAdmin
13	Administrativo
\.


--
-- Name: roles_rol_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.roles_rol_seq', 13, true);


--
-- Data for Name: roles_x_selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.roles_x_selectores (id, rol, selector) FROM stdin;
3	9	36
4	9	2
5	9	3
2	9	11
117	13	36
35	9	37
114	13	37
124	9	49
\.


--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.roles_x_selectores_id_seq', 124, true);


--
-- Data for Name: selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.selectores (id, superior, descripcion, ord, link) FROM stdin;
3	1	Roles	2	/sistema/rol/
11	1	Acceso menu	3	/sistema/selector/
36	1	Salir	7	/
1	0	Sistema	10	
2	1	Usuarios	1	/sistema/usuario/
37	1	Cambio de password	4	/aplicacion/cambiopass/
48	0	Aplicacion	20	
49	48	Desconexiones recibidas	0	/aplicacion/desconexionrecibida/
\.


--
-- Name: selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.selectores_id_seq', 49, true);


--
-- Data for Name: selectores_x_webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.selectores_x_webservice (id, selector, wservice) FROM stdin;
\.


--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.selectores_x_webservice_id_seq', 1, false);


--
-- Data for Name: usuarios; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.usuarios (usuario, cuenta, clave, token_iat) FROM stdin;
1	root	63a9f0ea7bb98050796b649e85481845	1613349298795
7	arnaldo	5ebe2294ecd0e0f08eab7690d2a6ee69	1606149423004
\.


--
-- Name: usuarios_usuario_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.usuarios_usuario_seq', 8, true);


--
-- Data for Name: usuarios_x_roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.usuarios_x_roles (id, usuario, rol) FROM stdin;
197	1	9
208	7	13
\.


--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.usuarios_x_roles_id_seq', 208, true);


--
-- Data for Name: webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.webservice (wservice, path, nombre) FROM stdin;
\.


--
-- Name: webservice_wservice_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.webservice_wservice_seq', 1, false);


--
-- Name: desconexiones_recibidas_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: hugo
--

ALTER TABLE ONLY aplicacion.desconexiones_recibidas
    ADD CONSTRAINT desconexiones_recibidas_pkey PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (rol);


--
-- Name: roles_x_selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_pkey PRIMARY KEY (id);


--
-- Name: selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores
    ADD CONSTRAINT selectores_pkey PRIMARY KEY (id);


--
-- Name: selectores_x_webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores_x_webservice
    ADD CONSTRAINT selectores_x_webservice_pkey PRIMARY KEY (id);


--
-- Name: usuarios_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (usuario);


--
-- Name: usuarios_x_roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_pkey PRIMARY KEY (id);


--
-- Name: webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.webservice
    ADD CONSTRAINT webservice_pkey PRIMARY KEY (wservice);


--
-- Name: roles_x_selectores_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_rol_fkey FOREIGN KEY (rol) REFERENCES sistema.roles(rol);


--
-- Name: roles_x_selectores_selector_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_selector_fkey FOREIGN KEY (selector) REFERENCES sistema.selectores(id);


--
-- Name: usuarios_x_roles_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_rol_fkey FOREIGN KEY (rol) REFERENCES sistema.roles(rol);


--
-- Name: usuarios_x_roles_usuario_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_usuario_fkey FOREIGN KEY (usuario) REFERENCES sistema.usuarios(usuario);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

