

function DesconexionRecibida(){
    
   this.tipo = "desconexionrecibida";   
   this.recurso = "desconexionesrecibidas";   
   this.value = 0;
   this.form_descrip = "";
   this.json_descrip = "";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
   
   
   this.titulosin = "Pedidos"
   this.tituloplu = "Pedidos"   
      
   
   this.campoid=  'id';
   this.tablacampos =  ['cuenta', 'documento', 'nombre_suscriptor', 'correo_suscriptor',
       'telefono_1',  'direccion_instalacion', 'nombre_nodo', 'nombre_comunidad'  ];
   
    this.etiquetas =  ['cuenta', 'documento', 'nombre_suscriptor', 'correo_suscriptor',
       'telefono_1',  'direccion_instalacion', 'nombre_nodo', 'nombre_comunidad'  ];
    
    
    this.tablaformat =['C', 'C', 'C', 'C',
       'C',  'C', 'C', 'C'  ];
   
   this.tbody_id = "desconexionrecibida-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "desconexionrecibida-acciones";   
      
   
   this.parent = null;
   

   
}





DesconexionRecibida.prototype.lista_new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);        
    
};





DesconexionRecibida.prototype.form_ini = function() {    

    
};



DesconexionRecibida.prototype.form_validar = function() {    
    return true;
};








DesconexionRecibida.prototype.post_form_id = function( obj  ) {                


                
    document.getElementById('btn_desconexionrecibida_nuevo').style.display = "none";
    document.getElementById('btn_desconexionrecibida_modificar').style.display = "none";
    document.getElementById('btn_desconexionrecibida_eliminar').style.display = "none";


    
    
};


