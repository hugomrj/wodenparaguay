/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.desconexion_recibida;

import java.util.Date;

/**
 *
 * @author hugo
 */
public class DesconexionRecibida {
    

  private Integer id;
  private String cosap;
  private Date fecha;
  private String franja_horaria ;
  private Integer gruop_id ;
  private String cuenta ;
  private String documento;
  private String nombre_suscriptor;
  private String correo_suscriptor;
  private String telefono_1;
  private String telefono_2;
  private String telefono_3;
  private String telefono_4;
  private String whatsapp;
  private String direccion_instalacion;
  private String nombre_nodo;
  private String nombre_comunidad;
  private String departamento;
  private String pais;
  private String serial_20;
  private String direccionable;
  private String familia;
  private String nombre_material;  
  private String fecha_dx;
  private String motivo_desconexion;
  private String observaciones;
  private String tipo_cliente;
  private String segmentacion;
  private String tipo_base;
  private String zona;
  private String agente_campo;
  private String nombre;
  private String latitud;
  private String longitud;
  private String prioridad;
  private String tipo_tarea;
  private String sede;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCosap() {
        return cosap;
    }

    public void setCosap(String cosap) {
        this.cosap = cosap;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getFranja_horaria() {
        return franja_horaria;
    }

    public void setFranja_horaria(String franja_horaria) {
        this.franja_horaria = franja_horaria;
    }

    public Integer getGruop_id() {
        return gruop_id;
    }

    public void setGruop_id(Integer gruop_id) {
        this.gruop_id = gruop_id;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNombre_suscriptor() {
        return nombre_suscriptor;
    }

    public void setNombre_suscriptor(String nombre_suscriptor) {
        this.nombre_suscriptor = nombre_suscriptor;
    }

    public String getCorreo_suscriptor() {
        return correo_suscriptor;
    }

    public void setCorreo_suscriptor(String correo_suscriptor) {
        this.correo_suscriptor = correo_suscriptor;
    }

    public String getTelefono_1() {
        return telefono_1;
    }

    public void setTelefono_1(String telefono_1) {
        this.telefono_1 = telefono_1;
    }

    public String getTelefono_2() {
        return telefono_2;
    }

    public void setTelefono_2(String telefono_2) {
        this.telefono_2 = telefono_2;
    }

    public String getTelefono_3() {
        return telefono_3;
    }

    public void setTelefono_3(String telefono_3) {
        this.telefono_3 = telefono_3;
    }

    public String getTelefono_4() {
        return telefono_4;
    }

    public void setTelefono_4(String telefono_4) {
        this.telefono_4 = telefono_4;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getDireccion_instalacion() {
        return direccion_instalacion;
    }

    public void setDireccion_instalacion(String direccion_instalacion) {
        this.direccion_instalacion = direccion_instalacion;
    }

    public String getNombre_nodo() {
        return nombre_nodo;
    }

    public void setNombre_nodo(String nombre_nodo) {
        this.nombre_nodo = nombre_nodo;
    }

    public String getNombre_comunidad() {
        return nombre_comunidad;
    }

    public void setNombre_comunidad(String nombre_comunidad) {
        this.nombre_comunidad = nombre_comunidad;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getSerial_20() {
        return serial_20;
    }

    public void setSerial_20(String serial_20) {
        this.serial_20 = serial_20;
    }

    public String getDireccionable() {
        return direccionable;
    }

    public void setDireccionable(String direccionable) {
        this.direccionable = direccionable;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getNombre_material() {
        return nombre_material;
    }

    public void setNombre_material(String nombre_material) {
        this.nombre_material = nombre_material;
    }

    public String getFecha_dx() {
        return fecha_dx;
    }

    public void setFecha_dx(String fecha_dx) {
        this.fecha_dx = fecha_dx;
    }

    public String getMotivo_desconexion() {
        return motivo_desconexion;
    }

    public void setMotivo_desconexion(String motivo_desconexion) {
        this.motivo_desconexion = motivo_desconexion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getTipo_cliente() {
        return tipo_cliente;
    }

    public void setTipo_cliente(String tipo_cliente) {
        this.tipo_cliente = tipo_cliente;
    }

    public String getSegmentacion() {
        return segmentacion;
    }

    public void setSegmentacion(String segmentacion) {
        this.segmentacion = segmentacion;
    }

    public String getTipo_base() {
        return tipo_base;
    }

    public void setTipo_base(String tipo_base) {
        this.tipo_base = tipo_base;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getAgente_campo() {
        return agente_campo;
    }

    public void setAgente_campo(String agente_campo) {
        this.agente_campo = agente_campo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getTipo_tarea() {
        return tipo_tarea;
    }

    public void setTipo_tarea(String tipo_tarea) {
        this.tipo_tarea = tipo_tarea;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
     
    
}
